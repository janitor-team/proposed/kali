# makefile for kali by Ed H. Chi (chi@geom.umn.edu) summer 1994
# $Id: Makefile,v 1.7 1996/10/07 17:04:37 slevy Exp $
#
SHELL = /bin/sh

KALIPKG = kali-3.1

KALI = kali
KALIPRINT = kaliprint
ALL = ${KALI} ${KALIPRINT}

BINDIR = /usr/local/bin

CC = cc

FORMSINCL = -I/u/share/include/XForms
FORMSLIB  = -L/usr/local/lib -lforms

# on Linux:
LIBDIRS = -L/usr/X11/lib
SYSLIBS = -lX11
SYSINCL =

# on Solaris:
#LIBDIRS = 
#SYSLIBS = -L/usr/openwin/lib -lX11 -lsocket -lnsl
#SYSINCL = -I/usr/openwin/include

# on SGI, Irix 5 or later:
#LIBDIRS =
#SYSLIBS = -lX11
#SYSINCL =

# on SGI, Irix 4:
#LIBDIRS =
#SYSLIBS = -lX11_s -lc_s
#SYSINCL =

INCLUDES = ${FORMSINCL} ${SYSINCL}
COPTS = -g
CFLAGS = -DXFORM ${COPTS} ${INCLUDES}
LIBS = ${LIBDIRS} ${FORMSLIB} ${SYSLIBS} -lm

#------------------------------------------------    sources
HDRS = help.h icons.h io.h kali.h main.h panel.h symmetry.h
SRCS = callbacks.c psio.c xio.c kali.c printmain.c panel.c symmetry.c panel.fd
COMMONOBJS = psio.o symmetry.o
KALIOBJS = kali.o callbacks.o xio.o ${COMMONOBJS}
PRINTOBJS = printmain.o ${COMMONOBJS}

BINS = sgi irix4 solaris linux

FILES = INSTALLATION README HELP ${SRCS} ${HDRS} Makefile data  ${BINS}

#------------------------------------------------    dependences

all: ${ALL}

kali.o : symmetry.h kali.h kali.c main.h io.h
symmetry.o : symmetry.h symmetry.c main.h
callbacks.o: panel.c panel.h icons.h help.h main.h
psio.o xio.o: symmetry.h io.h main.h kali.h
printmain.o : symmetry.h

help.h:	HELP
	(echo 'char *help[] = {'; \
	 sed  -e 's/"/\\"/g' -e 's/^$$/ /' -e 's/.*/"&",/' HELP; \
	 echo '"."};') > $@

#-----------------------------------------------    programs itself

${KALI}: ${KALIOBJS}
	${CC} -o $@ ${CFLAGS} ${KALIOBJS} ${LDFLAGS} ${LIBS}

${KALIPRINT}:  ${PRINTOBJS}
	${CC} -o $@ ${CFLAGS} ${PRINTOBJS} ${LDFLAGS} -lm

#----------------------------------------------     misc tasks
clean:
	rm -f *.BAK *.CKP a.out *.o || :

mrproper: clean
	rm -f ${ALL}

install: ${ALL}
	rm -f ${BINDIR}/${KALI} ${BINDIR}/${KALIPRINT}
	cp ${ALL}  ${BINDIR}

tar: _always
	rm -f ${KALIPKG}
	ln -s . ${KALIPKG}
	echo ${FILES} | tr " " "\12" | sed -e 's:^:${KALIPKG}/:' | gnutar -c -v -T - -f - | gzip -9 > ${KALIPKG}.tar.gz
	rm -f ${KALIPKG}

_always:
