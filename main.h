/*
 ** symmetry.h - Nina Amenta, Aug. 1989 *
 * main.h for kali  by Ed H. Chi (chi@geom.umn.edu)  summer 1994 
 * 
 * $Id: main.h,v 1.5 1996/10/07 15:40:42 slevy Exp $
 * $Log: main.h,v $
 * Revision 1.5  1996/10/07  15:40:42  slevy
 * Remove QREAD, etc., no longer needed.
 *
 * Revision 1.4  1996/09/30  21:02:40  slevy
 * Declare GXDraw instead of GLDraw.
 *
 * Revision 1.3  1996/06/04  17:26:08  slevy
 * Add DRAWER object -- table of function pointers,
 * with drawing routines, and push/pop/translate routines.
 * Add macros to call them easily.
 * Declare GLDraw, PSDraw, PickDraw instances.
 *
 * Revision 1.2  1994/09/12  23:14:41  chi
 * version 3.0 changes
 * header file recreated
 *
 * Revision 1.1  1994/08/26  16:29:02  chi
 * Initial revision
 *
 * Revision 1.1  1994/08/15  20:05:38  chi
 * Initial revision
 *

*/

#ifndef MAIN_H
#define MAIN_H


#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#define NUM_SYM 24

#define P3      0
#define P2      1
#define P1      2
#define PG      3
#define PGG     4
#define PMG     5
#define PM      6
#define CM      7
#define PMM     8
#define CMM     9
#define P31M    10
#define P3M1    11
#define P4      12
#define P4G     13
#define P4M     14
#define P6      15
#define P6M     16

/* Frieze symmetry groups */

#define HOP             17
#define SPINHOP         18
#define JUMP            19
#define SPINJUMP        20
#define SIDLE           21
#define SPINSIDLE       22
#define STEP            23

#define KALISX 0
#define KALISY 1
#define EX 2
#define EY 3
#define GLIDE 1

#define KALIDRAW 1
#define KALICUT 2
#define KALIPICK 3
#define KALITRANSFORM 4
#define KALIMOVE 5
#define KALIZOOM 6
#define KALIANGLE 7
#define KALIRATIO 8
#define KALIROTATE 9
#define KALIDELETE 10


#define NAMESTACKSIZE 50

typedef struct point_t  {
  float x;
  float y;
} POINT;


typedef struct rectangle_t  {
  float x;
  float y;
  float width;
  float height;
} RECTANGLE;

typedef POINT VECTOR;
typedef struct sym_t SYMMETRY;
typedef struct s_line LINE;
typedef float MATRIX[4];

typedef long WINDOW;
typedef long MENU;

/* Bit flags for dof (degrees of freedom) field of symmetry definition */
#define RAT 1
#define ANG 2

struct sym_t
{
  int dof;
  int trans;
  VECTOR v1;
  VECTOR v2;
  int refl;
  VECTOR reflections[2];
  int glide;
  int rot;
  char *label;
  LINE* grid;
};

struct s_line  {
  MATRIX m;
  short id;
  short obj_pos;
  LINE *next;
};

typedef struct _drawer DRAWER;
struct _drawer {
  char *kind;
  void (*drawobject)(DRAWER *, LINE *);
  void (*pushtfm)(DRAWER *);
  void (*translate)(DRAWER *, double x, double y);
  void (*poptfm)(DRAWER *);
  void (*drawline)(DRAWER *, LINE *);
  void (*drawpoints)(DRAWER *, POINT *, int);
  void *data;
};

#define TranslateCoordinates(drawer, x, y)    \
	{ if((drawer)->pushtfm) (*(drawer)->pushtfm)(drawer); \
	  (*(drawer)->translate)(drawer, x, y); \
	}

#define TranslateBack(drawer)		      \
	{ if((drawer)->poptfm) (*(drawer)->poptfm)(drawer); }

#define	DrawLine(drawer, line)		      \
	(*(drawer)->drawline)(drawer, line)

#define DrawObject(drawer, lines)		\
	(*(drawer)->drawobject)(drawer, lines)

#define	DrawPoints(drawer, points, count)			\
	{ if((drawer)->drawpoints) (*(drawer)->drawpoints)(drawer, points, count); }

extern DRAWER GXDraw;
extern DRAWER PSDraw;
extern DRAWER PickDraw;

#define RectIncludesPoint(r,p) \
  (((p).x>=(r).x) && \
   ((p).y>=(r).y) && \
   (((p).x<=(r).x+(r).width)) && \
   (((p).y<=(r).y+(r).height)))

#define PointRightOfRect(p,r) \
  (((p).x>(r).x+(r).width))

#define PointLeftOfRect(p,r) \
  (((p).x<(r).x))

#define PointAboveRect(p,r) \
  (((p).y>(r).y+(r).height))

#define PointBelowRect(p,r) \
  (((p).y<(r).y))


#define bump(src,dest,scale,vec) \
{  (dest)->x=(scale)*(vec)->x+(src)->x; (dest)->y=(scale)*(vec)->y+(src)->y; }

#endif
