/* Header file generated with fdesign. */

/**** Callback routines ****/

extern void PickProc(FL_OBJECT *, long);
extern void LineProc(FL_OBJECT *, long);
extern void LineProc(FL_OBJECT *, long);
extern void AngleProc(FL_OBJECT *, long);
extern void RatioProc(FL_OBJECT *, long);
extern void StartLoadProc(FL_OBJECT *, long);
extern void StartSaveProc(FL_OBJECT *, long);
extern void QuitProc(FL_OBJECT *, long);
extern void DrawProc(FL_OBJECT *, long);
extern void InfoProc(FL_OBJECT *, long);
extern void PrintProc(FL_OBJECT *, long);
extern void GridProc(FL_OBJECT *, long);
extern void DimProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SymmetryProc(FL_OBJECT *, long);
extern void SavePSProc(FL_OBJECT *, long);

extern void CloseThisPanel(FL_OBJECT *, long);



/**** Forms and Objects ****/

extern FL_FORM *KaliForm;

extern FL_OBJECT
        *MoveButton,
        *ZoomButton,
        *RotateButton,
        *AngleButton,
        *RatioButton,
        *LoadButton,
        *SaveButton,
        *QuitButton,
        *DrawButton,
        *InfoButton,
        *PrintButton,
        *GridButton,
        *DimChoice,
        *Frieze,
        *HopBitmap,
        *HopButton,
        *StepBitmap,
        *StepButton,
        *JumpBitmap,
        *JumpButton,
        *SpinJumpBitmap,
        *SpinJumpButton,
        *SidleBitmap,
        *SidleButton,
        *SpinSidleBitmap,
        *SpinSidleButton,
        *SpinHopBitmap,
        *SpinHopButton,
        *Wallpaper,
        *P6MBitmap,
        *PMMBitmap,
        *CMMBitmap,
        *P3Bitmap,
        *P2Bitmap,
        *PMGBitmap,
        *PGGBitmap,
        *CMBitmap,
        *P4Bitmap,
        *PMBitmap,
        *P6Bitmap,
        *P31MBitmap,
        *P4MBitmap,
        *P4GBitmap,
        *PGBitmap,
        *P1Bitmap,
        *P3M1Bitmap,
        *P6MButton,
        *P6Button,
        *P31MButton,
        *P4MButton,
        *P4GButton,
        *P4Button,
        *PGButton,
        *P3M1Button,
        *PMMButton,
        *CMMButton,
        *P3Button,
        *P2Button,
        *PMGButton,
        *PGGButton,
        *CMButton,
        *PMButton,
        *P1Button,
        *SavePSButton;

extern FL_FORM *InfoForm;

extern FL_OBJECT
        *InfoBrowser,
        *DoneButton;



/**** Creation Routine ****/

extern void create_the_forms();
