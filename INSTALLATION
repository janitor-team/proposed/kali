		    KALI 3.1

Binaries for kali and kaliprint are included under the directories
   "sgi"      (compiled under Irix 5.3, usable under Irix 5 and Irix 6)
   "irix4"    (compiled under Irix 4.0.5, usable under Irix 4 and 5)
   "solaris"  (compiled under Solaris 2.4 for Sparc machines) and
   "linux"    (compiled in ELF form under Linux 2.0 with gcc 2.7.2)

Here's how to compile your own binaries.

  Retrieve XForms for your machine.  It's available by anonymous FTP
  to laue.phys.uwm.edu under /pub/xforms.  You'll find various 
  .tgz (gzipped tar) files.

  The only files you'll need from the XForms package are
	xforms/FORMS/libforms.a  and
	xforms/FORMS/forms.h

  Edit the Makefile, changing the settings of FORMSINCL and FORMSLIB
  to refer to the directories where the above files are.

  On many systems, the XForms distribution includes both a static
  libforms.a library and a shared libforms.so.* library.  Unless you'll
  be permanently installing the libforms.so.* library, you should 
  remove or rename it while compiling kali, to ensure that kali is linked
  only with libforms.a.  Otherwise, you'll be unable to run kali,
  as it won't find the shared library at run time.

  Also edit the Makefile to uncomment or adjust the settings of LIBDIRS,
  SYSLIBS, and SYSINCL according to your type of system.  Check the setting of
  CC as well.  And, for "make install" to work, you may also want to
  adjust BINDIR if other than /usr/local/bin.

  Then, type "make" and/or "make install" to produce kali and kaliprint.

Documentation for kali appears in the file HELP, and on-line under the
"Info" button; there's no manual page.

Questions about kali can be sent to software@geom.umn.edu, or to the
authors as listed in the README file.
